// Antes de ejecutar este código tendremos que instalar sus dependencias con:
// npm install
// Atención: asegurarse de tener el fichero package.json para que sepa que módulos tiene que instalar el npm.

// Puerto en el que escuchará nuestro servidor:
var puerto = 9000;

// Creamos una nueva aplicación sobre la variable app, llamando a () sobre el objeto moduloexpress

var app = require('express')();

// Gracias al framework express podremos más adelante gestionar las peticiones http

// Para instalar socket.io es necesario que tengamos instanciado previamente un servidor http.
// Cargamos el módulo http y creamos un servidor con createServer()
// Como parámetro se le pasa una función con 2 parámetros (req,resp) o en este caso se le pasa
// una instancia de express, que es la forma sencilla de procesar las peticiones que lleguen al servidor.

var servidor = require('http').createServer(app);

// Por último queda cargar el módulo de socket.io y pasarle el servidor como parámetro.

var io = require('socket.io').listen(servidor);

// Comenzamos a aceptar peticiones en el puerto e ip especificadas:   .listen(puerto,[IP])
// Si no se indica IP, lo hará en todas las interfaces.

servidor.listen(puerto);
// Mostramos mensaje en la consola de Node.js:

console.log('Servidor de websockets escuchando en puerto: ' + puerto);

// Mediante express gestionamos las rutas de las peticiones http al servidor.
// Cuando  alguien haga una petición a la raiz del servidor web, le enviamos un texto de información.
// También se le podría enviar un fichero con  res.sendfile(__dirname + '/index.html');

app.get('/', function(req, res) {
    res.end('Servidor de websockets para Servicio Online de Canvas IES San Clemente.');
});


/////////////////////////////////////////////////////////////////////////////////////
////////////  PROGRAMACION DE LOS EVENTOS QUE GESTIONA EL SERVIDOR  /////////////////
//  https://github.com/LearnBoost/socket.io/wiki/Exposed-events
/////////////////////////////////////////////////////////////////////////////////////

io.sockets.on('connection', function(socket) {
    // Cada vez que se conecta un cliente mostramos un mensaje en la consola de Node.
    console.log("Nuevo cliente conectado..");

    // Crear 2 canales: avisarconserje e incidenciasresueltas

    //BORDE
    socket.on('borde',function(borde) {
        io.sockets.emit('borde',borde);
    });
    //COLOR
    socket.on('color',function(color) {
        io.sockets.emit('color',color);
    });
    //CLEAR
    socket.on('clear',function(){
        io.sockets.emit('clear');
    });
    //MOUSECLICK
    socket.on('mouseclick',function(pintar){
        io.sockets.emit('mouseclick',pintar);
    });
    //PAINT
    socket.on('paint',function(datos){
        io.sockets.emit('paint',datos);
    });
    //PINCELCOLOR
    socket.on('pincelcolor',function(colorpincel){
        io.sockets.emit('pincelcolor',colorpincel);
    });
    //PINCELBODY
    socket.on('pincelbody',function(pincelbody){
       io.sockets.emit('pincelbody',pincelbody);
    });
});
//Para ejecutar el servidor de websockets con Node.js escribir desde el terminal: node server.js
